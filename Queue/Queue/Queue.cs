﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Queue
{
    /// <summary>
    /// Очередь.
    /// </summary>
    /// <typeparam name="T">Тип элементов.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView))]
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public class Queue<T> : ICollection, IReadOnlyCollection<T>
    {
        private const int DefaultCapacity = 4;

        private T[] source;

        private int version;

        private readonly object syncRoot = new object();

        private int head, tail; // head - индекс первого элемента очереди, tail - индекс элемента, следующего за последним элементом очереди.

        private void ThrowIfEmpty()
        {
            if (Count == 0)
                throw new InvalidOperationException($"Свойство {nameof(Count)} было равно {Count}.");
        }

        private void ThrowIfNull<TObject>(TObject _object)
        {
            if (_object == null)
                throw new ArgumentNullException($"Параметр {nameof(_object)} был равен {_object}.");
        }

        private void ThrowIfInvalidArrayRange(Array array, int index)
        {
            ThrowIfNull(array);

            if (index < 0 || index >= array.Length)
                throw new IndexOutOfRangeException(nameof(index));

            if (Count > array.Length - index)
                throw new ArgumentException(nameof(index));
        }

        private void InternalCopyTo(Array array, int index)
        {
            if (head < tail)
                Array.Copy(source, head, array, index, Count);
            else
            {
                int firstPartCount = source.Length - head;
                Array.Copy(source, head, array, index, firstPartCount);
                Array.Copy(source, 0, array, index + firstPartCount, tail);
            }
        }

        private T GetByIndex(int i) => source[(head + i) % source.Length];

        /// <summary>
        /// Размер очереди.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Возвращает объект синхронизации.
        /// </summary>
        object ICollection.SyncRoot => syncRoot;

        /// <summary>
        /// Указывает синхронизирована ли коллекция.
        /// </summary>
        bool ICollection.IsSynchronized => false;

        /// <summary>
        /// Создаёт очередь с указанной ёмкостью.
        /// </summary>
        /// <param name="capacity">Ёмкость.</param>
        public Queue(int capacity = DefaultCapacity)
        {
            source = new T[DefaultCapacity];
        }

        /// <summary>
        /// Создаёт очередь из указанной коллекции.
        /// </summary>
        /// <param name="sequence">Коллекция.</param>
        public Queue(IEnumerable<T> sequence)
        {
            ThrowIfNull(sequence);

            source = new T[0];
            if (sequence is ICollection<T> collection)
            {
                Array.Resize(ref source, collection.Count);
                collection.CopyTo(source, 0);
                Count = collection.Count;
            }
            else
                foreach (var item in sequence)
                    Enqueue(item);
        }

        /// <summary>
        /// Создаёт пустую очередь.
        /// </summary>
        public Queue()
        {
            source = new T[DefaultCapacity];
        }

        /// <summary>
        /// Возвращает перечислитель очереди.
        /// </summary>
        /// <returns>Перечислитель очереди.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель очереди.
        /// </summary>
        /// <returns>Перечислитель очереди.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Копирует очередь в массив назначения.
        /// </summary>
        /// <param name="array">Массив назначения.</param>
        /// <param name="index">Индекс, с которого начинается копирование.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            ThrowIfInvalidArrayRange(array, index);

            if (array.Rank != 1)
                throw new ArgumentOutOfRangeException(nameof(array.Rank));

            InternalCopyTo(array, index);
        }

        /// <summary>
        /// Копирует очередь в массив назначения.
        /// </summary>
        /// <param name="array">Массив назначения.</param>
        /// <param name="index">Индекс, с которого начинается копирование.</param>
        public void CopyTo(T[] array, int index)
        {
            ThrowIfInvalidArrayRange(array, index);
            InternalCopyTo(array, index);
        }

        /// <summary>
        /// Возвращает перечислитель очереди.
        /// </summary>
        /// <returns>Перечислитель очереди.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Вставляет элемент в очередь.
        /// </summary>
        /// <param name="item">Вставляемый элемент.</param>
        public void Enqueue(T item)
        {
            int previousLength = source.Length;
            if (Count == source.Length)
            {
                Array.Resize(ref source, Count == 0 ? DefaultCapacity : source.Length * 2);
                if (Count > 0)
                {
                    T[] array = new T[source.Length];
                    int firstPartCount = source.Length - head;
                    Array.Copy(source, head, array, 0, firstPartCount);
                    Array.Copy(source, 0, array, firstPartCount, tail);
                    source = array;
                    tail = previousLength;
                    head = 0;
                }
            }
            source[tail] = item;
            tail = ++tail % source.Length;
            Count++;
            version++;
        }

        /// <summary>
        /// Вынимает элемент из очереди.
        /// </summary>
        /// <returns>Нижний элемент очереди.</returns>
        public T Dequeue()
        {
            ThrowIfEmpty();
            T result = source[head];
            source[head] = default(T);
            head = ++head % source.Length;
            Count--;
            version++;
            return result;
        }

        /// <summary>
        /// Возвращает нижний элемент очереди.
        /// </summary>
        /// <returns>Нижний элемент очереди.</returns>
        public T Peek()
        {
            ThrowIfEmpty();
            return source[head];
        }

        /// <summary>
        /// Проверяет присутствует ли в очереди указанный элемент.
        /// </summary>
        /// <param name="item">Искомый элемент.</param>
        /// <returns>True, если искомый элемент найден.</returns>
        public bool Contains(T item)
        {
            int count = Count;
            int i = source.Length - 1;
            IEqualityComparer<T> equalityComparer = EqualityComparer<T>.Default;
            while ((count > 0) && !equalityComparer.Equals(item, GetByIndex(i)))
            {
                count--;
                i++;
            }

            return count > 0;
        }

        /// <summary>
        /// Очищает очередь.
        /// </summary>
        public void Clear()
        {
            source = new T[source.Length];
            head = tail = 0;
            Count = 0;
            version++;
        }

        /// <summary>
        /// Перечислитель.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private Queue<T> queue;
            private int index;
            private readonly int version;

            public void ThrowWhenCollectionWasChanged()
            {
                if (version != queue.version)
                    throw new InvalidOperationException("Коллекция была изменена");
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Current;
                }
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            public T Current => queue.GetByIndex(index);

            /// <summary>
            /// Создаёт перечислитель для указанной очереди.
            /// </summary>
            /// <param name="queue">Очередь.</param>
            public Enumerator(Queue<T> queue)
            {
                this.queue = queue ?? throw new ArgumentNullException(nameof(queue));
                index = -1;
                version = queue.version;
            }

            /// <summary>
            /// Очищает все ресурсы, используемые объектом.
            /// </summary>
            public void Dispose()
            {
            }

            /// <summary>
            /// Перемещает индекс к следующему элементу очереди.
            /// </summary>
            /// <returns>Значение, указывающее возможно ли двигаться дальше к концу очереди.</returns>
            public bool MoveNext()
            {
                ThrowWhenCollectionWasChanged();

                index++;
                return index < queue.Count;
            }

            /// <summary>
            /// Переустанавливает индекс в -1.
            /// </summary>
            public void Reset()
            {
                ThrowWhenCollectionWasChanged();
                index = -1;
            }
        }
    }
}
