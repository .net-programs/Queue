﻿using System;
using System.Linq;

namespace Queue
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Queue<int> queue = new Queue<int>();
            foreach (var item in Enumerable.Range(1, 100))
                queue.Enqueue(item);

            int[] array = new int[queue.Count];
            queue.CopyTo(array, 0);

            Console.WriteLine("Массив:");
            foreach (var item in array)
                Console.WriteLine(item);

            Console.WriteLine("\nОчередь");
            foreach (var item in queue)
                Console.WriteLine(item);

            Console.ReadLine();
        }
    }
}